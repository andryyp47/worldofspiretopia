using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldofSpiretopia.Behaviour;
using WorldofSpiretopia.Behaviour.Move;

namespace WorldofSpiretopia.Mono
{
    public class MainCharacter : MonoBehaviour
    {
        public CharacterMovement movement;

        public ICharacterMove characterMovement;

        public CharacterController controller;

        public float speed, jump, gravityScale;

        private float health;

        private Attribute attribute;

        /// <summary>
        /// PERLU KALO LU BUTUH VARIABLE DARI CLASS LAEN
        /// </summary>
        public float Heatlh { get => health;}

        public float Speed { get => speed; }

        public Transform thisTransform;

        private void Start()
        {
            health = 10;

            movement = new CharacterMovement(controller, speed, jump, gravityScale, thisTransform);

            characterMovement = (ICharacterMove)movement;
        }

        private void Update()
        {
            characterMovement.Move();
        }


    }

}
