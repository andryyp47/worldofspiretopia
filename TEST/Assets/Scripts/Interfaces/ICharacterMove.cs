using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public interface ICharacterMove
    {
        void Move();
    }
}