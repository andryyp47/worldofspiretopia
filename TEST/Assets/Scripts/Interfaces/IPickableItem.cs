﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace WorldofSpiretopia.Item
{
    public interface IPickableItem
    {
        void PickItem(Transform character, Vector3 itemPickOffset);

        void DropItem();
    }
}


