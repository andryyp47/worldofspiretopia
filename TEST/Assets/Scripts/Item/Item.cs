﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Item
{
    public class Item : MonoBehaviour, IPickableItem
    {
        public void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Tower Base"))
            {
                transform.SetParent(other.GetComponentInParent<Transform>());

                var otherMesh = other.GetComponentInParent<MeshRenderer>();

                var mesh = gameObject.GetComponent<MeshRenderer>();

                transform.localPosition = Vector3.zero;

                transform.localRotation = Quaternion.identity;

                var yPos = transform.localPosition.y + mesh.bounds.size.y + otherMesh.bounds.size.y;

                transform.localPosition = new Vector3(transform.localPosition.x, yPos, transform.localPosition.z);

                gameObject.GetComponent<Rigidbody>().isKinematic = true;
            }
        }

        public void PickItem(Transform character, Vector3 itemPickOffset)
        {
            transform.SetParent(character);

            transform.localPosition = Vector3.zero;

            transform.localRotation = Quaternion.identity;

            transform.localPosition = itemPickOffset;

            gameObject.GetComponent<Collider>().enabled = false;

            gameObject.GetComponent<Rigidbody>().isKinematic = true;
        }

        public void DropItem()
        {
            transform.SetParent(null);

            gameObject.GetComponent<Collider>().enabled = true;

            gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}

