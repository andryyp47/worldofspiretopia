using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBAr : MonoBehaviour
{
    public Slider slider;

    private void OnEnable()
    {
        Action.OnHealthChange += UpdateHealthBar;
    }

    public void UpdateHealthBar(float value)
    {
        slider.value = value;
    }

}

public class TakeDamage :  MonoBehaviour
{
    private void Update()
    {
        Action.OnHealthChange.Invoke(100);
    }
}
