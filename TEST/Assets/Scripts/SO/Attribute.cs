using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
[CreateAssetMenu(fileName = "Attribute", menuName = "Data/Attribute")]
public class Attribute : ScriptableObject
{
    #region Property

    public float Speed { get => speed; set => value = speed; }

    public float Health { get; set; }

    #endregion

    #region Variable

    [SerializeField]
    private float speed = 0.0f;

    [SerializeField]
    private float health = 0.0f;

    #endregion

    #region Constructor

    #endregion

}
