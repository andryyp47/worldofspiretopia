﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldofSpiretopia.Item;

namespace WorldofSpiretopia.Behaviour
{
    public class CharacterPickItem : MonoBehaviour
    {
        [SerializeField]
        private IPickableItem item = null;

        [SerializeField]
        private Vector3 handOffset = Vector3.zero;

        [SerializeField]
        private bool itemAvailable = false;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Tower Base"))
            {
                Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), other.GetComponent<Collider>());
            }

            else if (other.CompareTag("Tower Part"))
            {
                item = other.GetComponent<IPickableItem>();
            }
        }

        private void OnTriggerStay(Collider other)
        {
            if (other.CompareTag("Tower Part"))
            {
                if (Input.GetKeyDown(KeyCode.F))
                {
                    if (itemAvailable)
                    {
                        item = other.GetComponent<IPickableItem>();

                        item.DropItem();

                        itemAvailable = false;
                    }

                    else if (item != null && !itemAvailable)
                    {
                        item.PickItem(gameObject.transform, handOffset);

                        itemAvailable = true;
                    }
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            item = null;
        }
    }
}

