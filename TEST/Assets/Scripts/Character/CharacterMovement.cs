﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldofSpiretopia.Behaviour.Move;

namespace WorldofSpiretopia.Behaviour
{
    public class CharacterMovement : ICharacterMove
    {
        [SerializeField]
        private CharacterController controller;
        private Vector3 playerVelocity;
        private bool groundedPlayer;
        private float playerSpeed;
        private float jumpHeight;
        private float gravityValue;

        private Transform thisTransform;

        public CharacterMovement (CharacterController _controller, float _speed, float _jumpHeight, float _gravityScale, Transform _thisGameobject)
        {
            controller = _controller;
            playerSpeed = _speed;
            jumpHeight = _jumpHeight;
            gravityValue = _gravityScale;
            thisTransform = _thisGameobject;
        }

        public void Move()
        {
            groundedPlayer = controller.isGrounded;
            if (groundedPlayer && playerVelocity.y < 0)
            {
                playerVelocity.y = 0f;
            }

            Vector3 move = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            controller.Move(move * Time.deltaTime * playerSpeed);

            if (move != Vector3.zero)
            {
                thisTransform.forward = move;
            }

            // Changes the height position of the player..
            if (Input.GetButtonDown("Jump") && groundedPlayer)
            {
                playerVelocity.y += Mathf.Sqrt(jumpHeight * -3.0f * gravityValue);
            }

            playerVelocity.y += gravityValue * Time.deltaTime;
            controller.Move(playerVelocity * Time.deltaTime);
        }
    }
}

