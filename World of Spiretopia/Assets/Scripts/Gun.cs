using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public float fireRate;
    public int damage;
    public Transform firePoint;
    public float timer;
    public PlayerInput playerInput;
    public bool onFireGun;

    public int bullet;
    public int bulletOnMagazine;
    public int maxReload;
    public int cadanganBullet;

    // Update is called once per frame
    private void Start()
    {
        playerInput = GetComponent<CharacterMain>().playerInput;
        cadanganBullet = bulletOnMagazine * maxReload;
        bullet = bulletOnMagazine;
    }

    private void Update()
    {
        timer += Time.deltaTime;
        if (onFireGun)
        {
            if (timer >= fireRate && bullet <= 0)
            {
                timer = 0;
                bullet -= 1;
                FireGun();
            }
        }
        playerInput.PlayerMain.Shoot.performed += x => StartFireGun();
        playerInput.PlayerMain.Shoot.canceled += x => StopFireGun();
        if (bullet == 0 || playerInput.PlayerMain.Shoot.triggered)
        {
            Reload();
        }
    }

    private void FireGun()
    {
        Ray ray = Camera.main.ViewportPointToRay(Vector3.one * 0.5f);

        //Debug.DrawRay(firePoint.position, firePoint.forward * 100, Color.red, 2f);
        Debug.DrawRay(ray.origin, ray.direction * 100, Color.red);

        //Ray ray = new Ray(firePoint.position, firePoint.forward);
        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo, 100))
        {
            var Damageable = hitInfo.collider.GetComponent<IDamageAble>();
            if (Damageable != null)
            {
                Damageable.TakeDamage(damage);
                Debug.Log("damage");
            }
        }
    }

    private void StartFireGun()
    {
        onFireGun = true;
    }

    private void StopFireGun()
    {
        onFireGun = false;
    }

    private void Reload()
    {
        int sisa = bulletOnMagazine - bullet;
        cadanganBullet -= sisa;
        bullet = bulletOnMagazine;
    }
}