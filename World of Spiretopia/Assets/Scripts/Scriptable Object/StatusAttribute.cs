using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Status Attribute", menuName = "ScriptableObjects/Status Attribute")]
public class StatusAttribute : ScriptableObject
{
    public string characterName;
    public float walkSpeed;
    public float runSpeed;
    public int maxHealth;

    public enum type { Defender, Destroyer, Healer };

    public type classType;
}