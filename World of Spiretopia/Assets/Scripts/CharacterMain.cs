using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using WorldofSpiretopia.Behaviour.Move;

public class CharacterMain : MonoBehaviour, IDamageAble
{
    public Slider healthSlider;
    public StatusAttribute statAttribute;
    public Transform cameraMainTransform;
    public PlayerInput playerInput;
    private CharacterMain charMain;
    private CharacterMovement charMove;
    private ICharacterMove iCharMove;

    public Rigidbody playerRigidbody;

    [Header("Character Movement"), Space]
    private float storeSpeed;

    public float walkSpeed;
    public float runSpeed;
    public float jumpHeight;
    public int jumpCounter;
    public int health;

    public LayerMask groundLayer;
    public float distToGround;

    private void Awake()
    {
        charMain = this;
        playerInput = new PlayerInput();
        health = statAttribute.maxHealth;
        healthSlider.maxValue = health;
        walkSpeed = statAttribute.walkSpeed;
        runSpeed = statAttribute.runSpeed;
    }

    private void OnEnable()
    {
        playerInput.Enable();
    }

    private void OnDisable()
    {
        playerInput.Disable();
    }

    private void Start()
    {
        charMove = new CharacterMovement(playerRigidbody, charMain);
        iCharMove = (ICharacterMove)charMove;
    }

    private void FixedUpdate()
    {
        healthSlider.value = health;
        Ray();

        iCharMove.Move(walkSpeed, runSpeed, jumpHeight, jumpCounter, cameraMainTransform, playerInput);
    }

    private void Ray()
    {
        if (Physics.Raycast(transform.position, Vector3.down, distToGround /*+ 0.1f*/, groundLayer))
        {
            jumpCounter = 0;
        }
    }

    public void TakeDamage(int damage)
    {
        charMain.health -= damage;
    }
}