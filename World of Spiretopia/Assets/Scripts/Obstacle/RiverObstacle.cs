using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Obstacle
{
    public class RiverObstacle : ObstacleBaseClass
    {
        public override void OnTriggerEnter(Collider c)
        {
            base.OnTriggerEnter(c);
        }

        public override void OnTriggerExit(Collider c)
        {
            base.OnTriggerExit(c);
        }
    }
}