using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WorldofSpiretopia.Behaviour.Move;

namespace WorldofSpiretopia.Behaviour.Obstacle
{
    public abstract class ObstacleBaseClass : MonoBehaviour, IObstacle
    {
        public float speedModifier;

        public virtual void Obstacle()
        {
        }

        public virtual void OnTriggerEnter(Collider c)
        {
            if (c.GetComponent<CharacterMain>())
            {
                CharacterMain charMain = c.GetComponent<CharacterMain>();
                charMain.runSpeed = (charMain.runSpeed * speedModifier) / 100;
                charMain.walkSpeed = (charMain.walkSpeed * speedModifier) / 100;
            }
        }

        public virtual void OnTriggerExit(Collider c)
        {
            if (c.GetComponent<CharacterMain>())
            {
                CharacterMain charMain = c.GetComponent<CharacterMain>();
                charMain.runSpeed = (charMain.runSpeed * 100) / speedModifier;
                charMain.walkSpeed = (charMain.walkSpeed * 100) / speedModifier;
            }
        }
    }
}