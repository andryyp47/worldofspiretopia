using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Obstacle
{
    public interface IObstacle
    {
        void Obstacle();
    }
}