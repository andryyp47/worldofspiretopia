using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public interface ICharacterWalk
    {
        void Walk(Vector2 _movementInput, float _walkSpeed, Transform _cameraMainTransform);
    }
}