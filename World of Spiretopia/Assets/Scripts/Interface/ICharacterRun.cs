using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public interface ICharacterRun
    {
        void Run(Vector2 _movementInput, float _runSpeed, Transform _cameraMainTransform);
    }
}