using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public interface ICharacterMove
    {
        void Move(float _walkSpeed, float _runSpeed, float _jumpHeight, int _jumpCounter, Transform _cameraMainTransform, PlayerInput _playerInput);
    }
}