using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public interface ICharacterJump
    {
        void Jump(float _jumpHeight);
    }
}