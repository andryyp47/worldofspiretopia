using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public class CharacterMovement : ICharacterMove
    {
        private CharacterMain charMain;
        private Rigidbody playerRigidbody;
        public float walkSpeed;
        public float runSpeed;
        public float jumpHeight;
        public float jumpCounter;

        private CharacterWalk charWalk;
        private ICharacterWalk iCharWalk;

        private CharacterRun charRun;
        private ICharacterRun iCharRun;

        private CharacterJump charJump;
        private ICharacterJump iCharJump;

        private const string HORIZONTAL = "Horizontal";
        private const string VERTICAL = "Vertical";
        private const string RUN = "Run";
        private const string JUMP = "Jump";

        public CharacterMovement(Rigidbody _playerRigidbody, CharacterMain _charMain)
        {
            playerRigidbody = _playerRigidbody;
            charMain = _charMain;

            charWalk = new CharacterWalk(playerRigidbody, this);
            iCharWalk = (ICharacterWalk)charWalk;

            charRun = new CharacterRun(playerRigidbody, this);
            iCharRun = (ICharacterRun)charRun;

            charJump = new CharacterJump(playerRigidbody, this);
            iCharJump = (ICharacterJump)charJump;
        }

        public void Move(float _walkSpeed, float _runSpeed, float _jumpHeight, int _jumpCounter, Transform _cameraMainTransform, PlayerInput _playerInput)
        {
            Vector2 movementInput = _playerInput.PlayerMain.Move.ReadValue<Vector2>();

            if (_playerInput.PlayerMain.Jump.triggered && charMain.jumpCounter < 2)
            {
                Debug.Log(_playerInput.PlayerMain.Jump.triggered);
                iCharJump.Jump(_jumpHeight);
                charMain.jumpCounter += 1;
            }

            if (_playerInput.PlayerMain.Run.triggered)
            {
                iCharRun.Run(movementInput, _runSpeed, _cameraMainTransform);
            }
            else
            {
                iCharWalk.Walk(movementInput, _walkSpeed, _cameraMainTransform);
            }
        }
    }
}