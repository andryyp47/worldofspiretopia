using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public class CharacterWalk : ICharacterWalk
    {
        private Rigidbody playerRigidbody;
        private CharacterMovement charMovement;

        public float turnSmoothTime = 0.1f;
        private float turnSmoothVelocity = 0.1f;

        public CharacterWalk(Rigidbody _playerRigidbody, CharacterMovement _charMovement)
        {
            playerRigidbody = _playerRigidbody;
            charMovement = _charMovement;
        }

        public void Walk(Vector2 _movementInput, float _walkSpeed, Transform _cameraMainTransform)
        {
            //Vector2 input = new Vector2(_axisHorizontal, _axisVertical);
            //Vector2 inputDir = input.normalized;
            if (_movementInput != Vector2.zero)
            {
                float targetRotation = Mathf.Atan2(_movementInput.x, _movementInput.y) * Mathf.Rad2Deg + _cameraMainTransform.eulerAngles.y;

                playerRigidbody.transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(playerRigidbody.transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);
            }
            //Quaternion target = Quaternion.Euler(playerRigidbody.transform.rotation.x, _cameraMainTransform.rotation.y, playerRigidbody.transform.rotation.z);
            //playerRigidbody.transform.rotation = target;
            //playerRigidbody.transform.rotation = Quaternion.Euler(playerRigidbody.transform.rotation.x, _cameraMainTransform.rotation.y * 180, playerRigidbody.transform.rotation.z);

            //playerRigidbody.transform.rotation = Quaternion.Euler(playerRigidbody.transform.eulerAngles.x, _cameraMainTransform.eulerAngles.y, playerRigidbody.transform.eulerAngles.z);

            //playerRigidbody.transform.Rotate(playerRigidbody.transform.rotation.x, _cameraMainTransform.rotation.y, playerRigidbody.transform.rotation.z);

            //playerRigidbody.MovePosition(_cameraMainTransform.forward + inputDir * _walkSpeed * Time.deltaTime);

            Vector3 direction = new Vector3(_movementInput.x, 0, _movementInput.y);
            //direction = _cameraMainTransform.rotation * direction;
            direction = _cameraMainTransform.rotation * direction;
            playerRigidbody.MovePosition(playerRigidbody.position + direction * _walkSpeed * Time.fixedDeltaTime);

            //playerRigidbody.velocity = new Vector3(inputDir.x * _walkSpeed, playerRigidbody.velocity.y, inputDir.z * _walkSpeed);
            //Vector3 move = (playerRigidbody.transform.forward * inputDir.y) * _walkSpeed * Time.deltaTime;
            //playerRigidbody.transform.Translate(move);
            //playerRigidbody.velocity =
            //playerRigidbody.velocity = playerRigidbody.transform.forward * (inputDir.z * _walkSpeed);
            //playerRigidbody.velocity = playerRigidbody.transform.right * (inputDir.x * _walkSpeed);
        }
    }
}