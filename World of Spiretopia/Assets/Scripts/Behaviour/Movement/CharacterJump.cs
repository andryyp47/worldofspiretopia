using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    
    public class CharacterJump : ICharacterJump
    {
        private Rigidbody playerRigidbody;
        private CharacterMovement charMovement;

        public CharacterJump(Rigidbody _playerRigidbody, CharacterMovement _charMovement)
        {
            playerRigidbody = _playerRigidbody;
            charMovement = _charMovement;
        }

        public void Jump(float _jumpHeight)
        {
            playerRigidbody.velocity = new Vector3(playerRigidbody.velocity.x, _jumpHeight, playerRigidbody.velocity.z);

        
        }
    }
}