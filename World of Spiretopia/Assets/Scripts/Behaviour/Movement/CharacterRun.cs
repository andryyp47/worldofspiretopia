using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WorldofSpiretopia.Behaviour.Move
{
    public class CharacterRun : ICharacterRun
    {
        private Rigidbody playerRigidbody;
        private CharacterMovement charMovement;

        public float turnSmoothTime = 0.1f;
        private float turnSmoothVelocity = 0.1f;

        public CharacterRun(Rigidbody _playerRigidbody, CharacterMovement _charMovement)
        {
            playerRigidbody = _playerRigidbody;
            charMovement = _charMovement;
        }

        public void Run(Vector2 _movementInput, float _runSpeed, Transform _cameraMainTransform)
        {
            //Vector2 input = new Vector3(_axisHorizontal, _axisVertical);
            //Vector2 inputDir = input.normalized;
            //playerRigidbody.velocity = new Vector3(inputDir.x * _runSpeed, playerRigidbody.velocity.y, inputDir.y * _runSpeed);
            if (_movementInput != Vector2.zero)
            {
                float targetRotation = Mathf.Atan2(_movementInput.x, _movementInput.y) * Mathf.Rad2Deg + _cameraMainTransform.eulerAngles.y;

                playerRigidbody.transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(playerRigidbody.transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);
            }
            Vector3 direction = new Vector3(_movementInput.x, 0, _movementInput.y);
            //direction = _cameraMainTransform.rotation * direction;
            direction = _cameraMainTransform.rotation * direction;
            playerRigidbody.MovePosition(playerRigidbody.position + direction * _runSpeed * Time.fixedDeltaTime);
        }
    }
}